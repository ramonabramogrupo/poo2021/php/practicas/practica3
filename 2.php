<?php

/**
 * OPCION 1
 */

// crear un array directamente
$vocales=[
    "a","e","i","o","u"
    ];

var_dump($vocales);

/**
 * OPCION 2
 */

// crear un array con la funcion array
$vocales=array(
    "a","e","i","o","u"
);

var_dump($vocales);

