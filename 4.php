<?php

$numeros=[
    "cero" => 0,
    "uno" => 1,
    "dos" => 2,
    "tres" => 3,
    "cuatro" => 4,
];

$vocales=[
    "a","e","i","o","u"
    ];

// leer el segundo elemento del array numeros y mostrarlo en pantalla (1)
echo $numeros["uno"];

// leer el segundo elemento del array vocales y mostrarlo en pantalla (e)
echo $vocales[1];

// añadir un elemento al final de vocales con la a con tilde

// opcion 1
$vocales[]="á";
// opcion 2
array_push($vocales,"á");

var_dump($vocales);


// añadir un elemento nuevo al array numeros con indice cinco con el valor 5
$numeros["cinco"]=5;

var_dump($numeros);



